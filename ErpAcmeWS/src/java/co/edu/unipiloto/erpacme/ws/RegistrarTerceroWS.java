/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ws;

import co.edu.unipiloto.erpacme.ejb.negocio.registrar.RegistrarBeanLocal;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author iyubinest
 */
@WebService(serviceName = "RegistrarTerceroWS")
public class RegistrarTerceroWS {

    @EJB
    RegistrarBeanLocal registrarBeanLocal;
    /**
     * Web service operation
     */
    @WebMethod(operationName = "registrarTerceroJuridicoWS")
    public Boolean registrarTerceroJuridicoWS(  @WebParam(name = "archivo") String archivo,
                                        @WebParam(name = "stream") byte[] fileBytes,
                                        @WebParam(name = "terIdentificacion") String terIdentificacion,
                                        @WebParam(name = "terRazonSocial") String terRazonSocial,
                                        @WebParam(name = "terEmail") String terEmail,
                                        @WebParam(name = "terTelefono") String terTelefono,
                                        @WebParam(name = "terDireccion") String terDireccion,
                                        @WebParam(name = "ciuCodigoCiudad") Integer ciuCodigoCiudad) {
        
        return registrarBeanLocal.registrar( archivo,
                                            "JU",
                                            "NIT",
                                            terIdentificacion,
                                            "",
                                            "",
                                            terRazonSocial,
                                            terEmail,
                                            terTelefono,
                                            terDireccion,
                                            ciuCodigoCiudad);
        
    }
    
    @WebMethod(operationName = "registrarTerceroNaturalWS")
    public Boolean registrarTerceroNaturalWS(  @WebParam(name = "archivo") String archivo,
                                        @WebParam(name = "terTipoIdentificacion") String terTipoIdentificacion,
                                        @WebParam(name = "terIdentificacion") String terIdentificacion,
                                        @WebParam(name = "terNombres") String terNombres,
                                        @WebParam(name = "terApellidos") String terApellidos,
                                        @WebParam(name = "terEmail") String terEmail,
                                        @WebParam(name = "terTelefono") String terTelefono,
                                        @WebParam(name = "terDireccion") String terDireccion,
                                        @WebParam(name = "ciuCodigoCiudad") Integer ciuCodigoCiudad) {
        
        return registrarBeanLocal.registrar( archivo,
                                            "NA",
                                            terTipoIdentificacion,
                                            terIdentificacion,
                                            terNombres,
                                            terApellidos,
                                            "",
                                            terEmail,
                                            terTelefono,
                                            terDireccion,
                                            ciuCodigoCiudad);
        
    }
    
    @WebMethod(operationName = "uploadWS")
    public Boolean uploadWS(    @WebParam(name = "archivo") String archivo,
                                @WebParam(name = "stream") byte[] fileBytes){
        return registrarBeanLocal.upload(archivo, fileBytes);
    }
    
}
