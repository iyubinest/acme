/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ws;

import co.edu.unipiloto.erpacme.ejb.negocio.actualizar.ActualizarBeanLocal;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author iyubinest
 */
@WebService(serviceName = "EstadoTerceroWS")
public class EstadoTerceroWS {

    @EJB
    ActualizarBeanLocal actualizarBeanLocal;

    /**
     * Web service operation
     */
    @WebMethod(operationName = "aprobar")
    public Boolean aprobar(@WebParam(name = "terceroCodigo") int terceroCodigo) {
        return actualizarBeanLocal.aprobar(terceroCodigo, Boolean.TRUE, "");
    }
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "rechazar")
    public Boolean rechazar(@WebParam(name = "terceroCodigo") int terceroCodigo,
                            @WebParam(name = "causaRechazo") String causaRechazo) {
        return actualizarBeanLocal.aprobar(terceroCodigo, Boolean.FALSE, causaRechazo);
    }
    
}
