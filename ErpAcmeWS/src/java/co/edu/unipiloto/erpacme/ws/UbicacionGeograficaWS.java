/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.unipiloto.erpacme.ws;

import co.edu.unipiloto.erpacme.ejb.negocio.dto.CiudadDTO;
import co.edu.unipiloto.erpacme.ejb.negocio.dto.DepartamentoDTO;
import co.edu.unipiloto.erpacme.ejb.negocio.ubicaciongeo.UbicacionGeograficaBeanLocal;
import co.edu.unipiloto.erpacme.ejb.negocio.util.CorreoElectronico;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.mail.MessagingException;
import javax.naming.NamingException;

/**
 *
 * @author SG701-21
 */
@WebService(serviceName = "UbicacionGeograficaWS")
public class UbicacionGeograficaWS {
    
    @EJB
    private UbicacionGeograficaBeanLocal ejbRef;

    @WebMethod(operationName = "consultarDepartamentosTodos")
    public List<DepartamentoDTO> consultarDepartamentosTodos() {
//        //Prueba envio correo
//        CorreoElectronico correoElectronico = new CorreoElectronico();
//        try {
//            correoElectronico.enviarMensaje("malagathaz@hotmail.com", "Prueba", "Prueba");
//        } catch (NamingException ex) {
//            Logger.getLogger(UbicacionGeograficaWS.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (MessagingException ex) {
//            Logger.getLogger(UbicacionGeograficaWS.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
        return ejbRef.consultarDepartamentosTodos();
    }    
    
}
