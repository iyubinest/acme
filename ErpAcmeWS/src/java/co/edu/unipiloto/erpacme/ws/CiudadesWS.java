/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ws;

import co.edu.unipiloto.erpacme.ejb.negocio.dto.CiudadDTO;
import co.edu.unipiloto.erpacme.ejb.negocio.ubicaciongeo.UbicacionGeograficaBeanLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author cristiangomez
 */
@WebService(serviceName = "CiudadesWS")
public class CiudadesWS {

    @EJB
    private UbicacionGeograficaBeanLocal ejbRef;
    
    @WebMethod(operationName = "consultarCiudadesXDepartamento")
    public List<CiudadDTO> consultarCiudadesXDepartamento(@WebParam(name = "codigoDepartamento") int codigoDepartamento) {
        //Prueba envio correo
        return ejbRef.consultarCiudadesXDepartamento(codigoDepartamento);
    }
}
