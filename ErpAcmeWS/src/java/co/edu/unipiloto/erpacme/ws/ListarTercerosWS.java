/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ws;

import co.edu.unipiloto.erpacme.ejb.negocio.dto.TerceroDTO;
import co.edu.unipiloto.erpacme.ejb.negocio.listar.ListarBeanLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;

/**
 *
 * @author iyubinest
 */
@WebService(serviceName = "ListarTercerosWS")
public class ListarTercerosWS {

    @EJB
    private ListarBeanLocal listarBeanLocal;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "consultarTercerosTodos")
    public List<TerceroDTO> consultarTercerosTodos() {
        return listarBeanLocal.listar();
    }
}
