/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ws;

import co.edu.unipiloto.erpacme.ejb.negocio.login.LoginBeanLocal;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author iyubinest
 */
@WebService(serviceName = "UsuarioWS")
public class UsuarioWS {
    
    @EJB
    LoginBeanLocal loginBeanLocal;
    
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "login")
    public Boolean login(   @WebParam(name = "user") String user,
                            @WebParam(name = "pass") String pass) {
        if(user.equals("admin") && pass.equals("admin")){
            return true;
        }else{
            return false;
        }
    }
    
    
    @WebMethod(operationName = "logout")
    public Boolean logout() {
        return true;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "loginTercero")
    public Boolean loginTercero(@WebParam(name = "terEmail") String terEmail, @WebParam(name = "terPassword") String terPassword) {
        return loginBeanLocal.login(terEmail, terPassword);
    }
}
