<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

$client=new SoapClient("http://localhost:8080/ErpAcmeWS/EstadoTerceroWS?WSDL"); 
$postdata = file_get_contents("php://input");
try {
    $request = (array) json_decode($postdata);
	if(isset($request['causaRechazo'])){
		$aprobar = false;
	}else{
		$aprobar = true;
	}

	if($aprobar==true){
		$res=$client->aprobar($request);
	}else{
		$res=$client->rechazar($request);
	}
	echo json_encode($res);
} catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
}
?>