'use strict';

/**
 * @ngdoc function
 * @name acmeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the acmeApp
 */
angular.module('acmeApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
