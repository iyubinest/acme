'use strict';

/**
 * @ngdoc function
 * @name acmeApp.controller:HomeAdCtrl
 * @description
 * # HomeAdCtrl
 * Controller of the acmeApp
 */
angular.module('acmeApp').controller('HomeAdCtrl', function ($scope, $http) {
    $scope.update = function(){
    	$http({
	        url: 'http://localhost:8888/listar.php'
	    })
	    .then(function(response){
	    	console.log(response);
	        $scope.terceros = response.data.return;
	    });
    };
    $scope.update();
    
    $scope.aprobar = function(id){
        $http({
            url: 'http://localhost:8888/estado.php',
            method: "POST",
            data: { 
                'terceroCodigo' : id 
            },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
        .then(function(response){
            $scope.update();
        });
    };

    $scope.rechazarModal = function(id){
        $scope.rechazarActual = id;
        $('#myModal').modal('show');
    };

    $scope.rechazar = function(){
        $scope.causaRechazo = '';
        if($scope.user.causa1){
            $scope.causaRechazo += 'Información insuficiente -';
        }
        if($scope.user.causa2){
            $scope.causaRechazo += 'Portafolio erroneo -';
        }
        if($scope.user.causa3){
            $scope.causaRechazo += 'Ya tenemos proveedores asociados a sus servicios -';
        }
        $http({
            url: 'http://localhost:8888/estado.php',
            method: "POST",
            data: { 
                'terceroCodigo' : $scope.rechazarActual,
                'causaRechazo' : $scope.causaRechazo
            },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
        .then(function(response){
            $scope.update();
        });
    };
});
