'use strict';

/**
 * @ngdoc function
 * @name acmeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the acmeApp
 */
angular.module('acmeApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
