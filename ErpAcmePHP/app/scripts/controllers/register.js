'use strict';

/**
 * @ngdoc function
 * @name acmeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the acmeApp
 */
angular.module('acmeApp')
  .controller('RegisterCtrl', function ($scope, $http) {
  	$scope.type=0;
    $scope.info = '';
    $scope.person_type = "Persona Juridica";
    $scope.document_type = "Tipo de Documento";

    $http({
        url: 'http://localhost:8888/departamentos.php'
    })
    .then(function(response){
        $scope.departamentos = response.data.return;
        $scope.departamentoActual = $scope.departamentos[0];
        $scope.updateDepartamento($scope.departamentoActual);
    });

    $scope.updateDepartamento = function(departamento){
        $scope.departamentoActual = departamento;
        $http({
            url: 'http://localhost:8888/ciudades.php',
            method: "POST",
            data: { 
                'codigoDepartamento' : departamento.depCodigoDepartamento 
            },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
        .then(function(response){
            $scope.ciudades = response.data.return;
            $scope.ciudadActual = $scope.ciudades[0];
        });
    };

    $scope.updateCiudad = function(ciudad){
        $scope.ciudadActual = ciudad;
    };

    $scope.updatePersonType = function(type){
    	$scope.type=type;
		if(type == 0){
			$scope.person_type = "Persona Juridica";
		}else{
			$scope.person_type = "Persona Natural";
		}
    };

    $scope.updateDocumentType = function(type){
		if(type == 0){
			$scope.document_type = "Cedula de Ciudadania";
		}else{
			$scope.document_type = "Cedula de Extranjeria";
		}
    };

    $scope.showname = function(){
    	if ($scope.type==0){
    		return true;
    	}else{
    		return false;
    	}
    };
    
    $scope.hidecamp = function(){
    	if ($scope.type==1){
    		return false;
    	}else{
    		return true;
    	}
    };

    $scope.registerUser = function(){
        $scope.user.ciuCodigoCiudad = $scope.ciudadActual.ciuCodigoCiudad;
        $scope.user.archivo = "";
        if($scope.person_type == "Persona Juridica"){
            $scope.user.terTipoPersona = "JU";
            $scope.user.terTipoIdentificacion = "NIT";
        }else{
            $scope.user.terTipoPersona = "NA";
            if($scope.document_type == "Cedula de Ciudadania"){
                $scope.user.terTipoIdentificacion = "CC";
            }else{
                $scope.user.terTipoIdentificacion = "CE";
            }
        }
        if ( $scope.user == undefined ){
            alert('completa el formulario');
            return;
        }
        if( $scope.user.terDireccion === '' || 
            $scope.user.terEmail === '' || 
            $scope.user.terTelefono === ''){
            alert('completa el formulario');
            return;
        }
        if ( $scope.user.terTipoPersona === "JU" ){
            if( $scope.user.terRazonSocial === '' || 
                $scope.user.terIdentificacion === ''){
                alert('completa el formulario');
                return;
            }
        } else if ( $scope.user.terTipoPersona === "NA" ){
            if( $scope.user.terNombres === '' || 
                $scope.user.terApellidos === '' || 
                $scope.user.terIdentificacion === ''){
                alert('completa el formulario');
                return;
            }
        }

        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        $http({
            url: 'http://localhost:8888/registrar.php',
            method: "POST",
            data: $scope.user,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
        .then(function(response){
            console.log(response);
            $('#myModal').modal('show');
        });
    }
  });
