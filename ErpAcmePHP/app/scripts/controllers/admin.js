'use strict';

/**
 * @ngdoc function
 * @name acmeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the acmeApp
 */
angular.module('acmeApp').controller('AdminCtrl', function ($scope, $http, $location) {
	$scope.login = function(){

		if ( $scope.user == undefined ){
            alert('completa el formulario');
            return;
        }
        if( $scope.user.user === '' || 
            $scope.user.admin === ''){
            alert('completa el formulario');
            return;
        }

		$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        $http({
            url: 'http://localhost:8888/login.php',
            method: "POST",
            data: $scope.user,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
        .then(function(response){
            if(response.data.return){
            	$location.url('/homeAd');
            } else {
            	alert("Datos errados");
            }
        });
	}; 
});
