<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

$client=new SoapClient("http://localhost:8080/ErpAcmeWS/UbicacionGeograficaWS?WSDL"); 
$res=$client->consultarDepartamentosTodos();
echo json_encode($res);
?>