<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

$client=new SoapClient("http://localhost:8080/ErpAcmeWS/RegistrarTerceroWS?WSDL"); 
$postdata = file_get_contents("php://input");
try {
	$postdata = file_get_contents("php://input");
	$request = (array) json_decode($postdata);

	if(isset($request['terRazonSocial'])){
		$natural = false;
	}else{
		$natural = true;
	}
    if($natural==false){
		$res=$client->registrarTerceroJuridicoWS($request);
	}else{
		$res=$client->registrarTerceroNaturalWS($request);
	}
	echo json_encode($res);
} catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
}
?>