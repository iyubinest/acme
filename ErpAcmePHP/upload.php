<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');
header('Content-Type: application/json');

$client=new SoapClient("http://localhost:8080/ErpAcmeWS/RegistrarTerceroWS?WSDL"); 
try {
	$request = array();
	$request["archivo"] = $_FILES["file"]["name"];
	$request["stream"] = file_get_contents($_FILES["file"]["tmp_name"]);
	$res=$client->uploadWS($request);
	echo json_encode($res);
} catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
}
?>