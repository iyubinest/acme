/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.registrar;

import co.edu.unipiloto.erpacme.ejb.negocio.dto.DepartamentoDTO;
import co.edu.unipiloto.erpacme.ejb.negocio.dto.TerceroDTO;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author SG701-21
 */
@Local
public interface RegistrarBeanLocal {

    /**
     * Metodo de negocio para registrar un tercero en la base de datos
     *
     * @return
     */
    public boolean registrar(   String archivo,
                                String terTipoPersona,
                                String terTipoIdentificacion,
                                String terIdentificacion,
                                String terNombres,
                                String terApellidos,
                                String terRazonSocial,
                                String terEmail,
                                String terTelefono,
                                String terDireccion,
                                Integer ciuCodigoCiudad);
    
    public boolean upload( String archivo, byte[] fileArray);
}
