/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.dto;

import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Ciudad;
import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Tercero;

/**
 *
 * @author iyubinest
 */
public class TerceroDTO {

    private String estado;
    private String archivo;
    private Integer terCodigoTercero;
    private String terTipoPersona;
    private String terTipoIdentificacion;
    private String terIdentificacion;
    private String terNombres;
    private String terApellidos;
    private String terRazonSocial;
    private String terEmail;
    private String terPassword;
    private String terDireccion;
    private String terTelefono;
    private Ciudad ciuCodigoCiudad;

    public TerceroDTO() {
    }

    public TerceroDTO(Tercero tercero) {
        this.estado = tercero.getTerEstado();
        this.archivo = tercero.getTerArchivo();
        this.terCodigoTercero = tercero.getTerCodigoTercero();
        this.terTipoPersona = tercero.getTerTipoPersona();
        this.terTipoIdentificacion = tercero.getTerTipoIdentificacion();
        this.terIdentificacion = tercero.getTerIdentificacion();
        this.terNombres = tercero.getTerNombres();
        this.terApellidos = tercero.getTerApellidos();
        this.terRazonSocial = tercero.getTerRazonSocial();
        this.terEmail = tercero.getTerEmail();
        this.terDireccion = tercero.getTerDireccion();
        this.terTelefono = tercero.getTerTelefono();
        this.ciuCodigoCiudad = tercero.getCiuCodigoCiudad();
        this.terPassword = tercero.getTerPassword();
    }

    public String getEstado() {
        return estado;
    }

    public String getArchivo() {
        return archivo;
    }

    public Integer getTerCodigoTercero() {
        return terCodigoTercero;
    }

    public String getTerTipoPersona() {
        return terTipoPersona;
    }

    public String getTerTipoIdentificacion() {
        return terTipoIdentificacion;
    }

    public String getTerIdentificacion() {
        return terIdentificacion;
    }

    public String getTerNombres() {
        return terNombres;
    }

    public String getTerApellidos() {
        return terApellidos;
    }

    public Ciudad getCiuCodigoCiudad() {
        return ciuCodigoCiudad;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public void setTerCodigoTercero(Integer terCodigoTercero) {
        this.terCodigoTercero = terCodigoTercero;
    }

    public void setTerTipoPersona(String terTipoPersona) {
        this.terTipoPersona = terTipoPersona;
    }

    public void setTerTipoIdentificacion(String terTipoIdentificacion) {
        this.terTipoIdentificacion = terTipoIdentificacion;
    }

    public void setTerIdentificacion(String terIdentificacion) {
        this.terIdentificacion = terIdentificacion;
    }

    public void setTerNombres(String terNombres) {
        this.terNombres = terNombres;
    }

    public void setTerApellidos(String terApellidos) {
        this.terApellidos = terApellidos;
    }

    public void setCiuCodigoCiudad(Ciudad ciuCodigoCiudad) {
        this.ciuCodigoCiudad = ciuCodigoCiudad;
    }

    public void setTerRazonSocial(String terRazonSocial) {
        this.terRazonSocial = terRazonSocial;
    }

    public String getTerRazonSocial() {
        return terRazonSocial;
    }

    public String getTerEmail() {
        return terEmail;
    }

    public String getTerDireccion() {
        return terDireccion;
    }

    public String getTerTelefono() {
        return terTelefono;
    }

    public void setTerEmail(String terEmail) {
        this.terEmail = terEmail;
    }

    public void setTerDireccion(String terDireccion) {
        this.terDireccion = terDireccion;
    }

    public void setTerTelefono(String terTelefono) {
        this.terTelefono = terTelefono;
    }

    public String getTerPassword() {
        return terPassword;
    }

    public void setTerPassword(String terPassword) {
        this.terPassword = terPassword;
    }
    
}
