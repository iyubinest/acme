/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.persistencia.dao;

import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Tercero;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author iyubinest
 */
@Stateless
@LocalBean
public class TerceroDAO {

    @PersistenceContext(unitName = "ErpAcmeEjbPU")
    private EntityManager entityManager;
    
    /**
     * Metodo que permite insertar un registro en la tabla Tercero
     *
     * @param tercero
     */
    public void insertarTercero(Tercero tercero) {
        entityManager.persist(tercero);
    }

    /**
     * Metodo que permite actualizar un registro existente en la tabla Tercero
     *
     * @param tercero
     */
    public void modificarTercero(Tercero tercero) {
        entityManager.merge(tercero);
    }

    /**
     * Metodo que permite eliminar un registro existente en la tabla Tercero
     *
     * @param tercero
     */
    public void eliminarTercero(Tercero tercero) {
        entityManager.remove(tercero);
    }

    /**
     * Metodo para consultar una Departamento por su llave primaria
     *
     * @param terceroCodigo
     * @return
     */
    public Tercero consultarTerceroXCodigo(int terceroCodigo) {
        return (Tercero) entityManager.find(Tercero.class, terceroCodigo);
    }

    /**
     * Metodo para consultar un departamento por su nombre
     *
     * @param nombreTercero
     * @return
     */
    public Tercero consultaTerceroXNombre(String nombreTercero) {
        Tercero tercero = new Tercero();

        //Definicion de la consulta a ejecutar
        String sql = "SELECT t FROM Tercero AS t "
                + "WHERE t.terNombre = :terNombre";

        //Generacion de la consulta
        Query consulta = entityManager.createQuery(sql);

        //Paso de parametros a la consulta
        consulta.setParameter("terNombre", nombreTercero);

        //Ejecutar la consulta
        tercero = (Tercero) consulta.getSingleResult();

        return tercero;
    }

    /**
     * Devuelve todos los departamentos existentes en el sistema
     *
     * @return
     */
    public List<Tercero> consultarTercerosTodos() {
        List<Tercero> terceros = new ArrayList<Tercero>();

        //Definir la consulta
        String sql = "SELECT t FROM Tercero AS t"
                    + " WHERE t.terEstado = :terEstado";

        //Genero la consulta
        Query consulta = entityManager.createQuery(sql);
        
        //Paso de parametros a la consulta
        consulta.setParameter("terEstado", "PENDIENTE");

        //Ejecuta la consulta
        terceros = consulta.getResultList();

        return terceros;
    }

    public boolean existeXEmailYPassword(String terEmail, String terPassword) {
        List<Tercero> terceros = new ArrayList<Tercero>();

        //Definir la consulta
        String sql = "SELECT t FROM Tercero AS t"
                    + " WHERE t.terEmail = :terEmail AND t.terPassword = :terPassword AND t.terEstado = :terEstado";

        //Genero la consulta
        Query consulta = entityManager.createQuery(sql);
        
        //Paso de parametros a la consulta
        consulta.setParameter("terEmail", terEmail);
        consulta.setParameter("terPassword", terPassword);
        consulta.setParameter("terEstado", "APROBADO");

        //Ejecuta la consulta
        terceros = consulta.getResultList();

        if(terceros.size()==1){
            return true;
        }else{
            return false;
        }
    }
    
}
