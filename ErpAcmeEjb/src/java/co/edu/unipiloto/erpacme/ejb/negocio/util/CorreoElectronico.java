/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.util;

import java.security.Security;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author iyubinest
 */
public class CorreoElectronico {

    /**
     * COnstructor por defect
     */
    public CorreoElectronico() {
    }

    public void enviarMensaje(String destinatario, String asunto, String mensaje) throws NamingException, MessagingException {
        Properties properties = new Properties();

        //Acceso al objeto representate del servidor para poder ubicar los servicios que este ofrece
        InitialContext initialContext = new InitialContext(properties);

        //Solicitar el servicio de correo
        Session mailSession = (Session) initialContext.lookup("email/ErpAcme");

        //Construir el mensaje que se va a enviar
        MimeMessage message = new MimeMessage(mailSession);
        message.setSubject(asunto);
        message.setRecipients(javax.mail.Message.RecipientType.TO, javax.mail.internet.InternetAddress.parse(destinatario, false));
        message.setText(mensaje);
        message.saveChanges();

        //Enviar el mensaje
        Transport.send(message);
    }

    private String mailhost = "smtp.gmail.com";

    public synchronized void sendMail(String subject, String body, String sender, String recipients)
            throws Exception {
        Properties properties = new Properties();
        InitialContext initialContext = new InitialContext(properties);
        Session mailSession = (Session) initialContext.lookup("email/ErpAcme");

        MimeMessage message = new MimeMessage(mailSession);
        message.setSender(new InternetAddress(sender));
        message.setSubject(subject);
        message.setContent(body, "text/plain");
        if (recipients.indexOf(',') > 0) {
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));
        } else {
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipients));
        }

        Transport.send(message);

    }
}
