/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.actualizar;

import co.edu.unipiloto.erpacme.ejb.negocio.dto.TerceroDTO;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author SG701-21
 */
@Local
public interface ActualizarBeanLocal {

    /**
     * Metodo de negocio para actualizar un tercero en la base de datos
     *
     * @param terceroCodigo
     * @param aprobado
     * @return
     */
    public Boolean aprobar(Integer terceroCodigo, Boolean aprobado , String extra);
}
