/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.listar;

import co.edu.unipiloto.erpacme.ejb.negocio.dto.TerceroDTO;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author SG701-21
 */
@Local
public interface ListarBeanLocal {

    /**
     * Metodo de negocio para registrar un tercero en la base de datos
     *
     * @return
     */
    public List<TerceroDTO> listar();
}
