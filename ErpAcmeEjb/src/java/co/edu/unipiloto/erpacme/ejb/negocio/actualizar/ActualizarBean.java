/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.actualizar;

import co.edu.unipiloto.erpacme.ejb.negocio.dto.TerceroDTO;
import co.edu.unipiloto.erpacme.ejb.negocio.util.CorreoElectronico;
import co.edu.unipiloto.erpacme.ejb.persistencia.dao.CiudadDAO;
import co.edu.unipiloto.erpacme.ejb.persistencia.dao.TerceroDAO;
import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Ciudad;
import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Tercero;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.naming.NamingException;

/**
 *
 * @author SG701-21
 */
@Stateless
public class ActualizarBean implements ActualizarBeanLocal {

    @EJB
    TerceroDAO terceroDAO;

    @Override
    public Boolean aprobar(Integer terceroCodigo, Boolean aprobado , String extra) {
        Tercero tercero = terceroDAO.consultarTerceroXCodigo(terceroCodigo);
        if(aprobado){
            tercero.setTerEstado("APROBADO");
            tercero.setTerPassword(passwordAleatorio());
            CorreoElectronico correoElectronico = new CorreoElectronico();
            try {
                correoElectronico.sendMail("APROBADO", "APROBADO, tu usuario es: "+tercero.getTerEmail()+" y tu password es: "+tercero.getTerPassword(),"erpacme1@gmail.com",tercero.getTerEmail());
            } catch (NamingException ex) {
                Logger.getLogger(CorreoElectronico.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MessagingException ex) {
                Logger.getLogger(CorreoElectronico.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(ActualizarBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            tercero.setTerEstado("RECHAZADO");
            CorreoElectronico correoElectronico = new CorreoElectronico();
            try {
                correoElectronico.sendMail("RECHAZADO", "RECHAZADO "+extra,"erpacme1@gmail.com",tercero.getTerEmail());
            } catch (NamingException ex) {
                Logger.getLogger(CorreoElectronico.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MessagingException ex) {
                Logger.getLogger(CorreoElectronico.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(ActualizarBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        terceroDAO.modificarTercero(tercero);
        return true;
    }

    private final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private Random rnd = new Random();
    private String passwordAleatorio(){
        StringBuilder sb = new StringBuilder( 8 );
        for( int i = 0; i < 8; i++ ) 
           sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }
}
