/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.unipiloto.erpacme.ejb.persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SG701-21
 */
@Entity
@Table(name = "ciudad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ciudad.findAll", query = "SELECT c FROM Ciudad c"),
    @NamedQuery(name = "Ciudad.findByCiuCodigoCiudad", query = "SELECT c FROM Ciudad c WHERE c.ciuCodigoCiudad = :ciuCodigoCiudad"),
    @NamedQuery(name = "Ciudad.findByCiuNombre", query = "SELECT c FROM Ciudad c WHERE c.ciuNombre = :ciuNombre")})
public class Ciudad implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ciu_codigo_ciudad")
    private Integer ciuCodigoCiudad;
    @Size(max = 200)
    @Column(name = "ciu_nombre")
    private String ciuNombre;
    @JoinColumn(name = "dep_codigo_departamento", referencedColumnName = "dep_codigo_departamento")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Departamento depCodigoDepartamento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ciuCodigoCiudad", fetch = FetchType.LAZY)
    private List<Tercero> terceroList;

    public Ciudad() {
    }

    public Ciudad(Integer ciuCodigoCiudad) {
        this.ciuCodigoCiudad = ciuCodigoCiudad;
    }

    public Integer getCiuCodigoCiudad() {
        return ciuCodigoCiudad;
    }

    public void setCiuCodigoCiudad(Integer ciuCodigoCiudad) {
        this.ciuCodigoCiudad = ciuCodigoCiudad;
    }

    public String getCiuNombre() {
        return ciuNombre;
    }

    public void setCiuNombre(String ciuNombre) {
        this.ciuNombre = ciuNombre;
    }

    public Departamento getDepCodigoDepartamento() {
        return depCodigoDepartamento;
    }

    public void setDepCodigoDepartamento(Departamento depCodigoDepartamento) {
        this.depCodigoDepartamento = depCodigoDepartamento;
    }

    @XmlTransient
    public List<Tercero> getTerceroList() {
        return terceroList;
    }

    public void setTerceroList(List<Tercero> terceroList) {
        this.terceroList = terceroList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ciuCodigoCiudad != null ? ciuCodigoCiudad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ciudad)) {
            return false;
        }
        Ciudad other = (Ciudad) object;
        if ((this.ciuCodigoCiudad == null && other.ciuCodigoCiudad != null) || (this.ciuCodigoCiudad != null && !this.ciuCodigoCiudad.equals(other.ciuCodigoCiudad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Ciudad[ ciuCodigoCiudad=" + ciuCodigoCiudad + " ]";
    }
    
}
