/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.registrar;

import co.edu.unipiloto.erpacme.ejb.persistencia.dao.CiudadDAO;
import co.edu.unipiloto.erpacme.ejb.persistencia.dao.TerceroDAO;
import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Ciudad;
import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Tercero;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author SG701-21
 */
@Stateless
public class RegistrarBean implements RegistrarBeanLocal {

    @EJB
    TerceroDAO terceroDAO;
    
    @EJB
    CiudadDAO ciudadDAO;

    /**
     * Metodo de negocio para registrar un tercero en la base de datos
     *
     * @param archivo
     * @param terTipoPersona
     * @param terTipoIdentificacion
     * @param terIdentificacion
     * @param terNombres
     * @param terApellidos
     * @param terRazonSocial
     * @param terEmail
     * @param terTelefono
     * @param terDireccion
     * @param ciuCodigoCiudad
     * @return
     */
    public boolean registrar(   String archivo,
                                String terTipoPersona,
                                String terTipoIdentificacion,
                                String terIdentificacion,
                                String terNombres,
                                String terApellidos,
                                String terRazonSocial,
                                String terEmail,
                                String terTelefono,
                                String terDireccion,
                                Integer ciuCodigoCiudad){
        Tercero tercero = new Tercero();
        tercero.setTerEstado("PENDIENTE");
        tercero.setTerArchivo(archivo);
        tercero.setTerTipoPersona(terTipoPersona);
        tercero.setTerTipoIdentificacion(terTipoIdentificacion);
        tercero.setTerIdentificacion(terIdentificacion);
        tercero.setTerNombres(terNombres);
        tercero.setTerApellidos(terApellidos);
        tercero.setTerRazonSocial(terRazonSocial);
        tercero.setTerDireccion(terDireccion);
        tercero.setTerEmail(terEmail);
        tercero.setTerTelefono(terTelefono);
        Ciudad ciudad = ciudadDAO.consultarCiudadXCodigo(ciuCodigoCiudad);
        if(ciudad!=null){
            tercero.setCiuCodigoCiudad(ciudad);
            terceroDAO.insertarTercero(tercero);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean upload(String archivo, byte[] fileArray) {
        String filePath = "e:/"+archivo;
        try {
            FileOutputStream fos = new FileOutputStream(filePath);
            BufferedOutputStream outputStream = new BufferedOutputStream(fos);
            outputStream.write(fileArray);
            outputStream.close(); 
            System.out.println("Received file: " + filePath);
            return true;
        } catch (IOException ex) {
            System.err.println(ex);
            return false;
        }
    }

    
}
