/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.unipiloto.erpacme.ejb.persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SG701-21
 */
@Entity
@Table(name = "departamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Departamento.findAll", query = "SELECT d FROM Departamento d"),
    @NamedQuery(name = "Departamento.findByDepCodigoDepartamento", query = "SELECT d FROM Departamento d WHERE d.depCodigoDepartamento = :depCodigoDepartamento"),
    @NamedQuery(name = "Departamento.findByDepNombre", query = "SELECT d FROM Departamento d WHERE d.depNombre = :depNombre")})
public class Departamento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "dep_codigo_departamento")
    private Integer depCodigoDepartamento;
    @Size(max = 200)
    @Column(name = "dep_nombre")
    private String depNombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "depCodigoDepartamento", fetch = FetchType.LAZY)
    private List<Ciudad> ciudadList;

    public Departamento() {
    }

    public Departamento(Integer depCodigoDepartamento) {
        this.depCodigoDepartamento = depCodigoDepartamento;
    }

    public Integer getDepCodigoDepartamento() {
        return depCodigoDepartamento;
    }

    public void setDepCodigoDepartamento(Integer depCodigoDepartamento) {
        this.depCodigoDepartamento = depCodigoDepartamento;
    }

    public String getDepNombre() {
        return depNombre;
    }

    public void setDepNombre(String depNombre) {
        this.depNombre = depNombre;
    }

    @XmlTransient
    public List<Ciudad> getCiudadList() {
        return ciudadList;
    }

    public void setCiudadList(List<Ciudad> ciudadList) {
        this.ciudadList = ciudadList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (depCodigoDepartamento != null ? depCodigoDepartamento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Departamento)) {
            return false;
        }
        Departamento other = (Departamento) object;
        if ((this.depCodigoDepartamento == null && other.depCodigoDepartamento != null) || (this.depCodigoDepartamento != null && !this.depCodigoDepartamento.equals(other.depCodigoDepartamento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Departamento[ depCodigoDepartamento=" + depCodigoDepartamento + " ]";
    }
    
}
