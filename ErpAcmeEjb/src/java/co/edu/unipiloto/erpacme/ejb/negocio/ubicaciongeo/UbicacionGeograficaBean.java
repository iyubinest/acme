/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.ubicaciongeo;

import co.edu.unipiloto.erpacme.ejb.negocio.dto.CiudadDTO;
import co.edu.unipiloto.erpacme.ejb.negocio.dto.DepartamentoDTO;
import co.edu.unipiloto.erpacme.ejb.persistencia.dao.CiudadDAO;
import co.edu.unipiloto.erpacme.ejb.persistencia.dao.DepartamentoDAO;
import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Ciudad;
import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Departamento;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author SG701-21
 */
@Stateless
public class UbicacionGeograficaBean implements UbicacionGeograficaBeanLocal {

    @EJB
    DepartamentoDAO departamentoDAO;
    
    @EJB
    CiudadDAO ciudadDAO;

    /**
     * Metodo de negocio para consultar todos los departamentos existentes en el
     * sistema
     *
     * @return
     */
    public List<DepartamentoDTO> consultarDepartamentosTodos() {
        List<DepartamentoDTO> departamentoDTOs = new ArrayList<>();

        //Consultar los departamentos desde la base de datos
        List<Departamento> departamentos = departamentoDAO.consultarDepartamentosTodos();

        //Recorrer el listado de departamentos y generar los DTOs
        for (Departamento departamento : departamentos) {
            DepartamentoDTO departamentoDTO = new DepartamentoDTO(departamento);
            departamentoDTOs.add(departamentoDTO);
        }

        return departamentoDTOs;
    }

    @Override
    public List<CiudadDTO> consultarCiudadesXDepartamento(Integer codigoDepartamento) {
        List<CiudadDTO> ciudadDTOs = new ArrayList<>();

        //Consultar los departamentos desde la base de datos
        List<Ciudad> ciudades = ciudadDAO.consultarCiudadesXDepartamento(codigoDepartamento);

        //Recorrer el listado de departamentos y generar los DTOs
        for (Ciudad ciudad : ciudades) {
            CiudadDTO ciudadDTO = new CiudadDTO(ciudad);
            ciudadDTOs.add(ciudadDTO);
        }

        return ciudadDTOs;
    }

}
