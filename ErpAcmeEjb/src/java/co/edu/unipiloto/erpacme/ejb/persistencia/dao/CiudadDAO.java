/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.persistencia.dao;

import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Ciudad;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author SG701-21
 */
@Stateless
@LocalBean
public class CiudadDAO {

    @PersistenceContext(unitName = "ErpAcmeEjbPU")
    private EntityManager entityManager;

    /**
     * Metodo que permite insertar un registro en la tabla Ciudad
     *
     * @param ciudad
     */
    public void insertarCiudad(Ciudad ciudad) {
        entityManager.persist(ciudad);
    }

    /**
     * Metodo que permite actualizar un registro existente en la tabla Ciudad
     *
     * @param ciudad
     */
    public void modificarCiudad(Ciudad ciudad) {
        entityManager.merge(ciudad);
    }

    /**
     * Metodo que permite eliminar un registro existente en la tabla Ciudad
     *
     * @param ciudad
     */
    public void eliminarCiudad(Ciudad ciudad) {
        entityManager.remove(ciudad);
    }

    /**
     * Metodo para consultar una ciudad por su llave primaria
     *
     * @param codigoCiudad
     * @return
     */
    public Ciudad consultarCiudadXCodigo(int codigoCiudad) {
        return (Ciudad) entityManager.find(Ciudad.class, codigoCiudad);
    }

    /**
     * Metodo para consultar una ciudad por su nombre
     *
     * @param nombreCiudad
     * @return
     */
    public Ciudad consultarCiudadXNombre(String nombreCiudad) {
        Ciudad ciudad = new Ciudad();

        //Definicion de la consulta a ejecutar
        String sql = "SELECT c FROM Ciudad AS c "
                + "WHERE c.ciuNombre = :ciuNombre";

        //Generacion de la consulta
        Query consulta = entityManager.createQuery(sql);

        //Paso de parametros a la consulta
        consulta.setParameter("ciuNombre", nombreCiudad);

        //Ejecutar la consulta
        ciudad = (Ciudad) consulta.getSingleResult();

        return ciudad;
    }

    /**
     * Devuelve todas las ciudades existentes en el sistema
     *
     * @return
     */
    public List<Ciudad> consultarCiudadesTodos() {
        List<Ciudad> ciudades = new ArrayList<Ciudad>();

        //Definir la consulta
        String sql = "SELECT c FROM Ciudad AS c";

        //Genero la consulta
        Query consulta = entityManager.createQuery(sql);

        //Ejecuta la consulta
        ciudades = consulta.getResultList();

        return ciudades;
    }

    /**
     * Devuelve las ciudades existentes en el sistema para un departamento
     *
     * @return
     */
    public List<Ciudad> consultarCiudadesXDepartamento(int codigoDepartamento) {
        List<Ciudad> ciudades = new ArrayList<Ciudad>();

        //Definir la consulta
        String sql = "SELECT c FROM Ciudad AS c "
                + "WHERE c.depCodigoDepartamento.depCodigoDepartamento = :codigoDepartamento ";

        //Generar la consulta
        Query consulta = entityManager.createQuery(sql);
        
        //Pasar los parametros
        consulta.setParameter("codigoDepartamento", codigoDepartamento);

        //Ejecutar la consulta
        ciudades = consulta.getResultList();

        return ciudades;
    }
}
