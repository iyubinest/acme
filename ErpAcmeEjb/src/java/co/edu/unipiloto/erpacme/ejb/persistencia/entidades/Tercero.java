/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.unipiloto.erpacme.ejb.persistencia.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SG701-21
 */
@Entity
@Table(name = "tercero")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tercero.findAll", query = "SELECT t FROM Tercero t"),
    @NamedQuery(name = "Tercero.findByTerCodigoTercero", query = "SELECT t FROM Tercero t WHERE t.terCodigoTercero = :terCodigoTercero"),
    @NamedQuery(name = "Tercero.findByTerTipoPersona", query = "SELECT t FROM Tercero t WHERE t.terTipoPersona = :terTipoPersona"),
    @NamedQuery(name = "Tercero.findByTerTipoIdentificacion", query = "SELECT t FROM Tercero t WHERE t.terTipoIdentificacion = :terTipoIdentificacion"),
    @NamedQuery(name = "Tercero.findByTerIdentificacion", query = "SELECT t FROM Tercero t WHERE t.terIdentificacion = :terIdentificacion"),
    @NamedQuery(name = "Tercero.findByTerRazonSocial", query = "SELECT t FROM Tercero t WHERE t.terRazonSocial = :terRazonSocial"),
    @NamedQuery(name = "Tercero.findByTerNombres", query = "SELECT t FROM Tercero t WHERE t.terNombres = :terNombres"),
    @NamedQuery(name = "Tercero.findByTerApellidos", query = "SELECT t FROM Tercero t WHERE t.terApellidos = :terApellidos")})
public class Tercero implements Serializable {
    @Size(max = 20)
    @Column(name = "ter_telefono")
    private String terTelefono;
    @Size(max = 100)
    @Column(name = "ter_email")
    private String terEmail;
    @Size(max = 200)
    @Column(name = "ter_estado")
    private String terEstado;
    @Size(max = 200)
    @Column(name = "ter_password")
    private String terPassword;
    @Size(max = 200)
    @Column(name = "ter_direccion")
    private String terDireccion;
    @Size(max = 300)
    @Column(name = "ter_archivo")
    private String terArchivo;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ter_codigo_tercero")
    private Integer terCodigoTercero;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "ter_tipo_persona")
    private String terTipoPersona;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "ter_tipo_identificacion")
    private String terTipoIdentificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ter_identificacion")
    private String terIdentificacion;
    @Size(max = 200)
    @Column(name = "ter_razon_social")
    private String terRazonSocial;
    @Size(max = 200)
    @Column(name = "ter_nombres")
    private String terNombres;
    @Size(max = 200)
    @Column(name = "ter_apellidos")
    private String terApellidos;
    @JoinColumn(name = "ciu_codigo_ciudad", referencedColumnName = "ciu_codigo_ciudad")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Ciudad ciuCodigoCiudad;

    public Tercero() {
    }

    public Tercero(Integer terCodigoTercero) {
        this.terCodigoTercero = terCodigoTercero;
    }

    public Tercero(Integer terCodigoTercero, String terTipoPersona, String terTipoIdentificacion, String terIdentificacion) {
        this.terCodigoTercero = terCodigoTercero;
        this.terTipoPersona = terTipoPersona;
        this.terTipoIdentificacion = terTipoIdentificacion;
        this.terIdentificacion = terIdentificacion;
    }

    public Integer getTerCodigoTercero() {
        return terCodigoTercero;
    }

    public void setTerCodigoTercero(Integer terCodigoTercero) {
        this.terCodigoTercero = terCodigoTercero;
    }

    public String getTerTipoPersona() {
        return terTipoPersona;
    }

    public void setTerTipoPersona(String terTipoPersona) {
        this.terTipoPersona = terTipoPersona;
    }

    public String getTerTipoIdentificacion() {
        return terTipoIdentificacion;
    }

    public void setTerTipoIdentificacion(String terTipoIdentificacion) {
        this.terTipoIdentificacion = terTipoIdentificacion;
    }

    public String getTerIdentificacion() {
        return terIdentificacion;
    }

    public void setTerIdentificacion(String terIdentificacion) {
        this.terIdentificacion = terIdentificacion;
    }

    public String getTerRazonSocial() {
        return terRazonSocial;
    }

    public void setTerRazonSocial(String terRazonSocial) {
        this.terRazonSocial = terRazonSocial;
    }

    public String getTerNombres() {
        return terNombres;
    }

    public void setTerNombres(String terNombres) {
        this.terNombres = terNombres;
    }

    public String getTerApellidos() {
        return terApellidos;
    }

    public void setTerApellidos(String terApellidos) {
        this.terApellidos = terApellidos;
    }

    public Ciudad getCiuCodigoCiudad() {
        return ciuCodigoCiudad;
    }

    public void setCiuCodigoCiudad(Ciudad ciuCodigoCiudad) {
        this.ciuCodigoCiudad = ciuCodigoCiudad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (terCodigoTercero != null ? terCodigoTercero.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tercero)) {
            return false;
        }
        Tercero other = (Tercero) object;
        if ((this.terCodigoTercero == null && other.terCodigoTercero != null) || (this.terCodigoTercero != null && !this.terCodigoTercero.equals(other.terCodigoTercero))) {
            return false;
        }
        return true;
    }

    public String getTerTelefono() {
        return terTelefono;
    }

    public String getTerEmail() {
        return terEmail;
    }

    public String getTerEstado() {
        return terEstado;
    }

    public String getTerDireccion() {
        return terDireccion;
    }

    public String getTerArchivo() {
        return terArchivo;
    }

    public void setTerTelefono(String terTelefono) {
        this.terTelefono = terTelefono;
    }

    public void setTerEmail(String terEmail) {
        this.terEmail = terEmail;
    }

    public void setTerEstado(String terEstado) {
        this.terEstado = terEstado;
    }

    public void setTerDireccion(String terDireccion) {
        this.terDireccion = terDireccion;
    }

    public void setTerArchivo(String terArchivo) {
        this.terArchivo = terArchivo;
    }

    public void setTerPassword(String terPassword) {
        this.terPassword = terPassword;
    }

    public String getTerPassword() {
        return terPassword;
    }

    @Override
    public String toString() {
        return "co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Tercero[ terCodigoTercero=" + terCodigoTercero + " ]";
    }

    
}
