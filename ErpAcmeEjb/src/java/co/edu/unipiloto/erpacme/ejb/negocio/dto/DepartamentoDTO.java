/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.dto;

import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Departamento;

/**
 *
 * @author SG701-21
 */
public class DepartamentoDTO {

    private Integer depCodigoDepartamento;
    private String depNombre;

    public DepartamentoDTO() {
    }
    
    public DepartamentoDTO(Departamento departamento) {
        this.depCodigoDepartamento = departamento.getDepCodigoDepartamento();
        this.depNombre = departamento.getDepNombre();
    }

    public Integer getDepCodigoDepartamento() {
        return depCodigoDepartamento;
    }

    public void setDepCodigoDepartamento(Integer depCodigoDepartamento) {
        this.depCodigoDepartamento = depCodigoDepartamento;
    }

    public String getDepNombre() {
        return depNombre;
    }

    public void setDepNombre(String depNombre) {
        this.depNombre = depNombre;
    }

    
}
