/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.persistencia.dao;

import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Ciudad;
import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Departamento;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author SG701-21
 */
@Stateless
@LocalBean
public class DepartamentoDAO {

    @PersistenceContext(unitName = "ErpAcmeEjbPU")
    private EntityManager entityManager;

    /**
     * Metodo que permite insertar un registro en la tabla Departamento
     *
     * @param departamento
     */
    public void insertarDepartamento(Departamento departamento) {
        entityManager.persist(departamento);
    }

    /**
     * Metodo que permite actualizar un registro existente en la tabla Departamento
     *
     * @param departamento
     */
    public void modificarDepartamento(Departamento departamento) {
        entityManager.merge(departamento);
    }

    /**
     * Metodo que permite eliminar un registro existente en la tabla Departamento
     *
     * @param departamento
     */
    public void eliminarDepartamento(Departamento departamento) {
        entityManager.remove(departamento);
    }

    /**
     * Metodo para consultar una Departamento por su llave primaria
     *
     * @param departamentoCiudad
     * @return
     */
    public Departamento consultarDepartamentoXCodigo(int codigoDepartamento) {
        return (Departamento) entityManager.find(Departamento.class, codigoDepartamento);
    }

    /**
     * Metodo para consultar un departamento por su nombre
     *
     * @param nombreDepartamento
     * @return
     */
    public Departamento consultaDepartamentoXNombre(String nombreDepartamento) {
        Departamento departamento = new Departamento();

        //Definicion de la consulta a ejecutar
        String sql = "SELECT d FROM Departamento AS d "
                + "WHERE d.depNombre = :depNombre";

        //Generacion de la consulta
        Query consulta = entityManager.createQuery(sql);

        //Paso de parametros a la consulta
        consulta.setParameter("depNombre", nombreDepartamento);

        //Ejecutar la consulta
        departamento = (Departamento) consulta.getSingleResult();

        return departamento;
    }

    /**
     * Devuelve todos los departamentos existentes en el sistema
     *
     * @return
     */
    public List<Departamento> consultarDepartamentosTodos() {
        List<Departamento> departamentos = new ArrayList<Departamento>();

        //Definir la consulta
        String sql = "SELECT d FROM Departamento AS d";

        //Genero la consulta
        Query consulta = entityManager.createQuery(sql);

        //Ejecuta la consulta
        departamentos = consulta.getResultList();

        return departamentos;
    }
    
}
