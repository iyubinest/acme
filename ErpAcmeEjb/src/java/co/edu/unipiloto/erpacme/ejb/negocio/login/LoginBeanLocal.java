/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.login;

import co.edu.unipiloto.erpacme.ejb.negocio.registrar.*;
import co.edu.unipiloto.erpacme.ejb.negocio.dto.DepartamentoDTO;
import co.edu.unipiloto.erpacme.ejb.negocio.dto.TerceroDTO;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author SG701-21
 */
@Local
public interface LoginBeanLocal {

    /**
     * Metodo de negocio para loguear un tercero con la base de datos
     *
     * @return
     */
    public boolean login(   String terEmail,
                                String terPassword);
    
}
