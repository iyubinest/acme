/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.listar;

import co.edu.unipiloto.erpacme.ejb.negocio.dto.TerceroDTO;
import co.edu.unipiloto.erpacme.ejb.persistencia.dao.TerceroDAO;
import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Tercero;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author SG701-21
 */
@Stateless
public class ListarBean implements ListarBeanLocal {

    @EJB
    TerceroDAO terceroDAO;

    /**
     * Metodo de negocio para listar todos los terceros pendientes
     *
     * @return
     */
    public List<TerceroDTO> listar(){
        List<TerceroDTO> tercerosDTO = new ArrayList();
        List<Tercero> terceros = terceroDAO.consultarTercerosTodos();
        for(Tercero tercero : terceros){
            tercerosDTO.add(new TerceroDTO(tercero));
        }
        return tercerosDTO;
    }

}
