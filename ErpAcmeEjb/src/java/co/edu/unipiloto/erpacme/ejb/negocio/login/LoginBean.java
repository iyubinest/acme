/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.login;

import co.edu.unipiloto.erpacme.ejb.negocio.registrar.*;
import co.edu.unipiloto.erpacme.ejb.persistencia.dao.CiudadDAO;
import co.edu.unipiloto.erpacme.ejb.persistencia.dao.TerceroDAO;
import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Ciudad;
import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Tercero;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author SG701-21
 */
@Stateless
public class LoginBean implements LoginBeanLocal {

    @EJB
    TerceroDAO terceroDAO;
    
    /**
     * Metodo de negocio para loguear un tercero con la base de datos
     *
     * @return
     */
    public boolean login(   String terEmail,
                                String terPassword){
        return terceroDAO.existeXEmailYPassword(terEmail,terPassword);
    }

    
}
