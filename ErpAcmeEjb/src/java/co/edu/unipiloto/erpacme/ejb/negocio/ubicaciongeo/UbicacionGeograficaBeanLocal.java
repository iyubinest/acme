/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.ubicaciongeo;

import co.edu.unipiloto.erpacme.ejb.negocio.dto.CiudadDTO;
import co.edu.unipiloto.erpacme.ejb.negocio.dto.DepartamentoDTO;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author SG701-21
 */
@Local
public interface UbicacionGeograficaBeanLocal {

    /**
     * Metodo de negocio para consultar todos los departamentos existentes en el
     * sistema
     *
     * @return
     */
    public List<DepartamentoDTO> consultarDepartamentosTodos();
    public List<CiudadDTO> consultarCiudadesXDepartamento(Integer codigoDepartamento);
    
}
