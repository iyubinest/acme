/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.unipiloto.erpacme.ejb.negocio.dto;

import co.edu.unipiloto.erpacme.ejb.persistencia.entidades.Ciudad;

/**
 *
 * @author SG701-21
 */
public class CiudadDTO {

    private Integer ciuCodigoCiudad;
    private String ciuNombre;
    private DepartamentoDTO departamentoDTO;

    public CiudadDTO() {
    }

    public CiudadDTO(Ciudad ciudad) {
        this.ciuCodigoCiudad = ciudad.getCiuCodigoCiudad();
        this.ciuNombre = ciudad.getCiuNombre();

        if (ciudad.getDepCodigoDepartamento() != null
                && ciudad.getDepCodigoDepartamento().getDepCodigoDepartamento().intValue() > 0) {
            this.departamentoDTO = new DepartamentoDTO();
            this.departamentoDTO.setDepCodigoDepartamento(ciudad.getDepCodigoDepartamento().getDepCodigoDepartamento());
        }
    }

    public Integer getCiuCodigoCiudad() {
        return ciuCodigoCiudad;
    }

    public void setCiuCodigoCiudad(Integer ciuCodigoCiudad) {
        this.ciuCodigoCiudad = ciuCodigoCiudad;
    }

    public String getCiuNombre() {
        return ciuNombre;
    }

    public void setCiuNombre(String ciuNombre) {
        this.ciuNombre = ciuNombre;
    }

    public DepartamentoDTO getDepartamentoDTO() {
        return departamentoDTO;
    }

    public void setDepartamentoDTO(DepartamentoDTO departamentoDTO) {
        this.departamentoDTO = departamentoDTO;
    }

}
